﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrashInfo: MonoBehaviour {
	public enum TrashType { Glass, Paper, Bio, Plastic, Other, Hazardous, Length };
	public string definition;
	public string name;
	public Sprite image;
	public TrashType type;
}
