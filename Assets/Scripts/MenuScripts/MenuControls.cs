﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuControls : MonoBehaviour {
    
    string secretKey = "fiOjqbBUOuXJjVPCaCc6rmlpN7nWg5CF"; 
    public string addScoreUrl = "http://localhost:8888/get.php?";

    static string[] videos = new string[] { "embalaza.mp4", "papir.mp4", "bioloski.mp4", "steklo.mp4", "odpadki.mp4", "tovornjak.mp4" };

    public GameObject mainMenu;
	public GameObject pauseMenu;
    public GameObject gameOverMenu;
    public GameObject continueMenu;
    public GameObject modesMenu;
    public GameObject trashInfo;
	public GameObject infoMenu;
	public GameObject outside;
    public Transform livesParent;


    public RectTransform scrollView, viewport;

    public GameObject leaderboard;
    public Text Score;
    public GameObject mute, mute2;
    public GameObject tutorial;
    public Transform lines;

	AudioSource audioSource;
	public AudioClip continueClip;
	public AudioClip buttonPressClip;

    GameObject[] lives;

	public List<GameObject> lastMenus;

    // When the game starts
    // Opens up the main menu
    void Start() {
        lastMenus = new List<GameObject>();
        ShowMainMenu ();
        Time.timeScale = 0;
        audioSource = transform.GetComponent<AudioSource>();

        lives = new GameObject[livesParent.childCount];

        for (int i = 0; i < livesParent.childCount; i++)
            lives[i] = livesParent.GetChild(i).gameObject;

        var textComponents = Component.FindObjectsOfType<Text>();

        if (!PlayerPrefs.HasKey("firsttime")) {
            PlayerPrefs.SetInt("firsttime", 1);


        }
        if (!PlayerPrefs.HasKey("soundon")) {
            PlayerPrefs.SetInt("soundon", 1);

        }

        if (PlayerPrefs.GetInt("firsttime") > 0)
            tutorial.GetComponent<Animator>().enabled = true;

        if (PlayerPrefs.GetInt("soundon") == 0) {
            mute.SetActive(true);
            mute2.SetActive(true);
        }

        if (!PlayerPrefs.HasKey("score")) {
            PlayerPrefs.SetInt("score", 0);


        }
        Game.matrix = new int[6, 6];
        MakeMatrix();

    }

	//Shows the main menu and adds it to the menu queue
	public void ShowMainMenu(){
        ShowWindow(mainMenu, false);
	}

    public void SetLives(int l){
        for (int i = 0; i < lives.Length; i++){
            if (i >= l){
                lives[i].SetActive(false);
            }
            else
                lives[i].SetActive(true);

        }
            
    }

	void PlaySound(AudioClip clip){
		audioSource.clip = clip;
       
		audioSource.Play ();
	}

	public void ButtonPress(){
		PlaySound (buttonPressClip);
	}

    private void StartMode(int type) {
        Game.SetGametype((int)type);
        CloseLastAllMenus();
    }

    public void ChooseDefaultMode() {
        StartMode(GameType.DEFAULT);
        SetLives(5);
    }

    public void ChooseTeachingMode()
    {
        StartMode(GameType.TEACHING);
    }
    public void ChooseAllMode()
    {
        StartMode(GameType.ALL);
    }

	//Closes the last active menu
	//Doesn't close main menu if player presses outside of menu
	public void CloseLastMenu(bool force = false){

        if (lines.parent.gameObject.activeSelf) {
            lines.parent.gameObject.SetActive(false);
            return;
        }
        
        if (lastMenus.Count > 0 && (!lastMenus[lastMenus.Count - 1].Equals(mainMenu) && !lastMenus[lastMenus.Count - 1].Equals(continueMenu) && !lastMenus[lastMenus.Count - 1].Equals(gameOverMenu) 
            || force == true))
        {
			ButtonPress ();
			lastMenus[lastMenus.Count-1].SetActive (false);

			if (!lastMenus [lastMenus.Count - 1].Equals (continueMenu)) {
				lastMenus.RemoveAt (lastMenus.Count - 1);
				//lastMenus.RemoveAt (lastMenus.Count - 1);
                
                //ShowGameOverMenu ();

			}
		}
		if (lastMenus.Count == 0) {
			Time.timeScale = 1;
            outside.SetActive(false);
        }
	}

    public void CloseLastAllMenus(){
        while (lastMenus.Count > 0)
        {
            CloseLastMenu(true);
        }
        outside.SetActive(false);
    }

	//Shows the pause menu and adds it to the menu queue
	public void ShowPauseMenu(){
        if (Time.timeScale == 1)
        {
            ShowWindow(pauseMenu, false);
            ButtonPress();
        }
	}

    public void ToggleSound(){
        bool active = mute.activeInHierarchy;
        mute.SetActive(!active);
        mute2.SetActive(!active);
        AudioListener.pause = !active;
        int t = active == true ? 1 : 0;
        PlayerPrefs.SetInt("soundon", t);
    }

	//Shows the game over menu and adds it to the menu queue
    public void ShowGameOverMenu()
    {
        ShowWindow(gameOverMenu, false);
        Score.text = "Rezultat: " + Game.m_score;

        if (PlayerPrefs.GetInt("score") < Game.m_score)
            PlayerPrefs.SetInt("score", Game.m_score);

        SendScore(PlayerPrefs.GetInt("score"));
      

    }

	//Shows the continue menu and adds it to the menu queue
    public void ShowContinueMenu()
    {
        ShowWindow(continueMenu, false);
		StartCoroutine (ContinueCountdown ());
    }

    //Shows the trash info menu and adds it to the menu queue
    public void ShowTrashInfo(Trash trash)
    {
        Debug.Log("smo v trashInfotu");
        Text[] texts = trashInfo.transform.GetComponentsInChildren<Text>();
        foreach (Text t in texts)
        {
            if (t.gameObject.name.Equals("Definition"))
            {
                t.text = trash.m_description;
                Debug.Log("info: " + t.text);
            }
            else if (t.gameObject.name.Equals("Name"))
            {
                t.text = trash.m_title;
                Debug.Log("info: " + t.text);
            }
        }
        Image[] images = trashInfo.transform.GetComponentsInChildren<Image>();
        foreach (Image i in images)
        {
            if (i.gameObject.name.Equals("TrashImage"))
            {
                i.sprite = trash.m_image;
                Debug.Log("info: sprite");
            }
        }
        ShowWindow(trashInfo, false);
		ButtonPress ();
    }

    //Shows the info menu with all the different trash and adds it to the menu queue
    public void ShowInfoMenu(){
        ShowWindow(infoMenu, false);
		ButtonPress ();
	}

    public void ShowLeaderboard()
    {
        ShowWindow(leaderboard, false);
		ButtonPress ();
    }

    public void ShowModesMenu()
    {
        ShowWindow(modesMenu, false);
        ButtonPress();
    }

    public void ShowWindow(GameObject window, bool play) {
        window.SetActive(true);
        lastMenus.Add(window);
        window.transform.SetAsLastSibling();
        Time.timeScale = play?1:0;

        outside.SetActive(true);

    }
    
    //Switches tabs in the info menu
    public void SwitchTab(int tab) {
        scrollView.GetComponent<ScrollRect>().content = viewport.GetChild(3).GetComponent<RectTransform>();
        for (int i = 0; i < viewport.transform.childCount; i++) {
            //Debug.Log(viewport.GetChild(i).transform.gameObject.name);
            if (i == tab) {
                viewport.GetChild(i).transform.gameObject.SetActive(true);
                scrollView.GetComponent<ScrollRect>().content = viewport.GetChild(i).GetComponent<RectTransform>();
            }
            else {
                viewport.GetChild(i).transform.gameObject.SetActive(false);
            }
        }
		ButtonPress ();
    }


    //Changes the text in the coundown menu
    //IF it drops to 0 changes menu to game over
    public Text countdownText;
	IEnumerator ContinueCountdown(){
		for (int i = 3; i > 0; i--) {
			countdownText.text = i.ToString();
			PlaySound (continueClip);
            yield return new WaitForSecondsRealtime(1);
		}
		if (continueMenu.active == true) {
			lastMenus.RemoveAt (lastMenus.IndexOf (continueMenu));
               
			continueMenu.SetActive (false);
            ShowGameOverMenu ();

		}
	}

   
    public void ResetGame() {
        for (int i = 0; i < Trashes.activeTrashes.Count; i++) {
            Destroy(Trashes.activeTrashes[i].m_transform.gameObject);
        }
        Trashes.activeTrashes.Clear();
        

        Game.ResetSpeed();
        Game.ResetScore();
        TrashGenerator.Reset();

        //CloseLastMenu();
        ShowWindow(mainMenu, false);
    }



    public void PlayTutorial() {
        //andheld.PlayFullScreenMovie("uvodniVideo.mp4", Color.black, FullScreenMovieControlMode.Full);
        PlayerPrefs.SetInt("firsttime", 0);
        tutorial.GetComponent<Animator>().enabled = false;
        tutorial.GetComponent<RectTransform>().localScale = Vector3.one;
    }

    
    public void PlayMovie() {

        //Handheld.PlayFullScreenMovie("bioloski.mp4", Color.black, FullScreenMovieControlMode.Minimal);
        for (int i = 0; i < Trashes.activeTrashes.Count; i++) {
            Trashes.activeTrashes[i].m_transform.position = Trashes.activeTrashes[i].m_transform.position - Vector3.right * 7;
        }
        //CloseLastAllMenus();
        lastMenus.RemoveAt(lastMenus.IndexOf(continueMenu));

        continueMenu.SetActive(false);
        outside.SetActive(false);
        Time.timeScale = 1;
        Game.matrix = new int[6 , 6];
    }

    public void ShowMatrix() {
        MakeMatrix();
        lines.parent.gameObject.SetActive(true);
        lines.parent.SetAsLastSibling();
    }

    
    public void MakeMatrix() {

        StringBuilder sb = new StringBuilder();    
        for (int i = 0; i < lines.childCount; i++) {
            Text t = lines.GetChild(i).GetComponent<Text>();
            t.text = "";
            string s = "";
            for (int j = 0; j < 5; j++) {
                int k = Game.matrix[i, j];
                if (k < 10)
                    s= string.Format("{0, -12}", k);
                else
                    s= string.Format("{0, -11}", k);
                if (i == j)
                    s = "<color=green>" + s + "</color>";

                t.text += s;

            }

            int k2 = Game.matrix[i, 5];
            if (k2 < 10)
                s = string.Format("{0}", k2);
            else
                s = string.Format("{0}", k2);
            if (i==5)
                s = "<color=green>" + s + "</color>";
            t.text += s;


        }

    }

    public void SendScore(int score) {
        
        StartCoroutine(PostScores(score));
    }

    // remember to use StartCoroutine when calling this function!
    IEnumerator PostScores(int score) {

        string hash = Md5Sum(score + secretKey);
        string post_url = addScoreUrl + "&score=" + score + "&hash=" + hash;

        // Post the URL to the site and create a download object to get the result.
        WWW hs_post = new WWW(post_url);
        yield return hs_post; // Wait until the download is done
        if (hs_post.error != null) {
            print("There was an error posting the high score: " + hs_post.error);
        }
    }

    public string Md5Sum(string strToEncrypt) {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++) {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }

    public void OpenFb(){
        Application.OpenURL("https://www.facebook.com/FriCycle/");
    }

}
