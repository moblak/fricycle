﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CoroutineUtils {

	//Waits for a float amount of seconds regardless of timeScale
	public static IEnumerator WaitForRealSeconds(float time)
	{
		float start = Time.realtimeSinceStartup;
		while (Time.realtimeSinceStartup < start + time)
		{
			yield return null;
		}
	}
}
