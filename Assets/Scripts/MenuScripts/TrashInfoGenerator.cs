﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

class TrashInfoGenerator : MonoBehaviour {
    public MenuControls menuControls;
    public GameObject infoPrefab;

    // index pove kater tip smeti gledamo (100 ... organic)
    public int index;

    Transform gen;

    public void Start()
    {
        gen = transform;
        //Debug.Log("smo v trashInfoGenerator " + index);

        int i = 0;
        while (i < Trashes.trashes.Count && Trashes.trashes[i].m_trash_index < index) i++;

        while (i < Trashes.trashes.Count && Trashes.trashes[i].m_trash_index < index+100) {
            GameObject go = GameObject.Instantiate(infoPrefab, gen);
            go.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            // Debug.Log("delamo infote: " + Trashes.trashes[i].m_title);


            Trash t = new Trash(Trashes.trashes[i]);
            t.setTransform(go.transform);


            Image[] images = go.transform.GetComponentsInChildren<Image>();
            foreach (Image im in images) {
                if (im.gameObject.name.Equals("TrashImage")) {
                    im.sprite = t.m_image;
                }
            }

            Button button = go.GetComponent<Button>();
            button.onClick.AddListener(delegate { menuControls.ShowTrashInfo(t); });

            i++;
        }
    }
}

