﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ScoreLevel : MonoBehaviour {

	public Text score, level;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float fScore = Game.m_score;
        float fSpeed = Game.speed;
		score.text = "Rezultat: " + fScore;
		level.text = "Hitrost: " + fSpeed;
	}
}
