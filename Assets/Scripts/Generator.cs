﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour {

    public GameObject trashPrefab;
    public Canvas canvas;
    public GameObject smokePrefab;
    public Transform smokePoint;
    public AudioSource wrong;
    MenuControls menuControls;

    [Header("Velikost smeti")]
    public float scale = 1;

    float t = 0;
    Transform gen;
    void Start () {
        gen = transform;
        menuControls = canvas.GetComponent<MenuControls>();
	}
	
	
	void Update () {
		//Ta if stavek je dodan samo, da se to ne pregleduje več ko se menuji odprejo. TODO: Optimize
        if (menuControls.lastMenus.Count == 0) {
			t += Time.deltaTime;
			if (t >= Game.interval) {
                //generiraj smet
                if ((Trashes.activeTrashes.Count > 0 && Trashes.PopLast() > -8.7f) || Trashes.activeTrashes.Count == 0) {
                    genTrash();
                    t = 0;
                }

			}

			//premikaj smeti (hitrsot je v TrashType)
			for (int i = 0; i < Trashes.activeTrashes.Count; i++) {

                Trashes.activeTrashes[i].m_transform.Translate (Vector3.right * Game.speed * Time.deltaTime, Space.World);
                
                if (Trashes.activeTrashes[i].m_transform.position.x > 9.7f) {

                    wrong.Play();
					Trashes.activeTrashes.RemoveAt (0);
					Destroy (gen.GetChild (i).gameObject);
                    int lives = Game.DecLives();
                    menuControls.SetLives(lives);
                    if (lives == 0)
                        menuControls.ShowGameOverMenu();
				}
			}
		}
	}


    void genTrash() {
        //drugi parameter je parent objekta
        GameObject go = GameObject.Instantiate(trashPrefab,  gen);

        //v active trash se doda object, ki se mu pripne transform realnega objekta
        Trash t = TrashGenerator.GenerateTrash();

        // GameType = ALL ... ko smo pregledali vse smeti
        if (t == null) return;

        Trashes.activeTrashes.Add(t);
        t.setTransform(go.transform);
        
      

        //skaliraj primerno, slike so lahko razlicno velike
        Vector3 size = go.GetComponent<SpriteRenderer>().bounds.size;
        
        if (size.x == 0) {
            print(t.m_image.name + "   WRONG NAME");
        }

        float defSize = size.x;
        if (size.y > size.x)
            defSize = size.y;
        
        go.transform.localScale = Vector3.one * scale / defSize;
        size = go.GetComponent<SpriteRenderer>().bounds.size;
        defSize = size.x;
        if (size.y > size.x)
            defSize = size.y;

        //nastavi kje na sirini traka se generira
        float randomize =Random.Range(-Game.tapeWidth / 2 + defSize/2, Game.tapeWidth / 2-defSize/2) +Game.tapeY;
        go.transform.localPosition = Vector3.zero + Vector3.left * 10 + Vector3.up*randomize; 
        go.transform.localRotation = Quaternion.Euler(0, 0, Random.Range(0,360));

        GameObject smoke = Instantiate(smokePrefab);

        smoke.transform.position = new Vector3(smokePoint.position.x, randomize, smokePoint.position.z);
    }
}
