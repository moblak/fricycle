﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TrashGenerator {

    public static int m_index;  // uporablja se ko igramo GameType.ALL
    public static bool gameover = false;
    public static void Reset() {
        m_index = 0;
        gameover = false;
    }

    public static void Init() {
        Trashes.InitTrashes();
        Trashes.InitPrefabs();
        Trashes.InitWeights();
    }
    public static Trash GenerateTrash() {

        // GameType.DEFAULT
        if (Game.GetGameType() == GameType.DEFAULT)
            return new Trash(Trashes.trashes[Random.Range(0, Trashes.trashes.Count-1)]);


        // GameType.TEACHING
        else if (Game.GetGameType() == GameType.TEACHING)
        {
            double random = (double)Random.Range(0.0f, 1.0f);
            //Debug.Log("random: " + random);
            for (int i = 0; i < Trashes.weights.Count; i++)
            {
                random -= Trashes.weights[i];
                //Debug.Log("random: " + random + " index: " + i + " weight: " + Trashes.weights[i]);
                if (random < 0)
                {
                    return new Trash(Trashes.trashes[i]);
                }
            }
            return new Trash(Trashes.trashes[Trashes.trashes.Count-1]);
        }

        // GameType.ALL
        else if (Game.GetGameType() == GameType.ALL)
        {
            if (Trashes.randomTrashes.Count - 227 > m_index) {
                Trash t = new Trash(Trashes.randomTrashes[m_index]);
                m_index++;
                return t;
            } else {
                gameover = true;
                return null; // Pregledane so bile vse smeti
            }
        }
        return null;
    }
}
