﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeControl : MonoBehaviour {

    public ParticleSystem system;

	void Update () {
        if (system.isStopped)
            Destroy(this);
	}
}
