﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Trashes
{

    public static List<Trash> trashes = new List<Trash>();       // V tem seznamu hranimo vse smeti 
    public static List<Trash> activeTrashes = new List<Trash>(); // trenutno na zaslonu
    public static List<Trash> randomTrashes = new List<Trash>(); // GameType.ALL
    public static List<double> weights = new List<double>();     // GameType.TEACHING

    // Metoda ki doda v naš seznam vse smeti
    public static void InitTrashes()
    {


        string[] desc = {
            // organic
            "Čeprav so papirnati robčki in brisače narejeni iz iste primarne surovine kot papir (celuloza), vseeno ne sodijo v zabojnik za papir in karton. Odlagati jih moramo med biološke odpadke, saj so razgradljivi. Ob deževnem dnevu potrebuje žepni robec za razgradnjo celo manj kot en dan.",
            "Ostanki hrane ter olupki sadja in zelenjave zavzemajo velik delež odpadkov naših gospodinstvih. Z njihovim ločevanjem prihranimo veliko prostora, poleg tega pa manj bioloških odpadkov na odlagališču pomeni tudi manj toplogrednih plinov.",
            "Če so organski odpadki pomešani z ostalimi odpadki, povzročajo velike količine toplogrednih plinov, nastanek nevarnih organskih kislin in neprijeten vonj na odlagališčih. V centralnem odlagališču takšne odpadke biološko predelajo.",

            // plastic
            "Plastenke narejene iz PET (Polietilen tereftalat) so reciklirane za ponovno uporabo materialov, iz katerih so plastenke narejene in za zmanjšanje količin odpadkov na odlagališčih.",
            "Pločevinka je narejena iz aluminija. To je surovina, ki jo je mogoče velikokrat reciklirati. Več kot 50% pločevink je recikliranih.",
            "Embalaža iz tetrapaka spada v zabojnik za embalažo, saj poleg papirja vsebuje tudi aluminij in polietilen.  Zato postopek recikliranja ni enak, kot pri papirju.",
            
            // glass
            "Steklo je možno 100% reciklirati in ponovno uporabiti, pri tem pa ne izgubi kakovosti. Ena tona odpadnega stekla nadomesti 1,2 tone novih surovin, ob tem pa prihranimo tudi veliko energije.",
            "Kozarec je možno 100% reciklirati in ponovno uporabiti, pri tem pa steklo ne izgubi kakovosti. Ena tona odpadnega stekla nadomesti 1,2 tone novih surovin, ob tem pa prihranimo tudi veliko energije.",

            // paper
            "Recikliran časopis je največkrat ponovno uporabljen kot papir za časopis. Uporabi pa se lahko tudi za druge izdelke iz papirja, kot so škatle za jajca, toaletni papir, izolirni material, papirnate brisače in podobno.",
            "Revije se lahko reciklirajo, vendar postopek ni enak kot pri časopisnem papirju, saj vsebujejo plast kaolina, ki daje svetlikajoč videz.",
            "Knjige imajo dolgo življenjsko dobo. Zato poglejmo, kaj še lahko storimo s knjigo, preden jo zavržemo.",
            "Papir za zavijanje daril se lahko reciklira, vendar je postopek okolju precej neprijazen, saj tovrstni papir vsebuje dodatke, kot so umetno zlato, bleščice, plastika.",
            "So popolnoma razgradljive, iz reciklata pa lahko izdelamo vrsto novih papirnatih izdelkov. Paziti moramo le, da ne vsebujejo druge snovi, zaradi česar jih je težko ali celo nemogoče reciklirati.",
            "Osnovna sestavina papirja je celuloza, ki jo pridobivamo iz lesa.1 tona recikliranega papirja reši 17 majhnih ali dve veliki drevesi. Stopnja recikliranja papirja v Sloveniji znaša 62,4 %.",

            // other
            "Cigaretni ogorki so sicer razgradljivi, a ne sodijo med biološke odpadke. Vsebujejo namreč strupe, ki so zdravju nevarni ob kompostiranju.",
            "Ker naj bi bile plenice koži in naravi prijazne, večina ljudi zmotno misli, da sodijo med biološko razgradljive odpadke.  A so zaradi zagotavljanja udobja in zaščite zunanji sloji sestavljeni iz vse prej kot naravnih, nerazgradljivih materialov. Zato sodijo med preostanek odpadkov.",
            "Britvica je narejena iz različnih materialov, največkrat vsaj iz plastike in jekla. Ker je pretežno iz plastike, veliko ljudi narobe misli, da sodi v zabojnik za embalažo. Uporabljeno britvico pravilno odvržemo v koš za preostanek odpadkov.",
            "Določene manjše predmete, ki jih ne potrebujemo več, največkrat odvržemo v smetnjak za preostanek odpadkov. Ob tem moramo paziti, iz kakšnih materialov so, saj so lahko v celoti plastični, leseni ali morda stekleni. V takšnih primerih jih moramo odložiti v primeren zabojnik. Če je predmet večji, sodi med kosovne odpadke in ga zapeljemo do odvoza.",
            "Uporabljene vrečke za sesalec sodijo med ostale odpadke, saj so narejene iz različnih umetnih materialov, poleg tega pa je tudi njihova vsebina velikokrat zelo raznovrstna."

        };


        #region ORGANIC
        trashes.Add(new Trash(TrashType.ORGANIC, 101, "Olupek banane", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/Banana_Peel"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 102, "Brokoli", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/brokoli"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 103, "List hrasta", desc[2], Resources.Load<Sprite>("Images/Trashes/Organic/hrast"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 104, "Jabolko4", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/jabolko4"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 105, "Olupek banane", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/banana"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 106, "Jabolko", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/jabolko"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 107, "Jabolko", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/jabolko1"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 108, "Jabolko", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/jabolko2"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 109, "Jabolko", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/jabolko3"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 110, "Jabolko", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/jabolko5"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 111, "Jajce", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/jajce"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 112, "Sadje", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/sadje"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 118, "Sadje", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/sadje6"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 114, "Sadje", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/sadje2"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 115, "Sadje", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/sadje3"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 116, "Sadje", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/sadje4"), 10));
        trashes.Add(new Trash(TrashType.ORGANIC, 117, "Sadje", desc[1], Resources.Load<Sprite>("Images/Trashes/Organic/sadje5"), 10));
        #endregion

        #region PLASTIC
        trashes.Add(new Trash(TrashType.PLASTIC, 201, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/emb_modro"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 202, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/1"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 203, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/2"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 204, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/3"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 205, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/4"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 206, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/emb_belo"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 207, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/emb_modro"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 208, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/emb_modro1"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 209, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/emb_zeleno"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 210, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza2"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 211, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza1"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 212, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza3"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 213, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza4"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 214, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza5"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 215, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza6"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 216, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza7"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 217, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza8"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 218, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza9"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 219, "Embalaža kozmetike", "Plastična embalaža kozmetike sodi v zabojnik za plastiko.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza10"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 220, "Embalaža kozmetike", "Plastična embalaža kozmetike sodi v zabojnik za plastiko.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza11"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 221, "Embalaža kozmetike", "Plastična embalaža kozmetike sodi v zabojnik za plastiko.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza12"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 222, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza13"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 223, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza14"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 224, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza15"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 225, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza16"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 226, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza17"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 227, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza18"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 228, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza19"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 229, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza20"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 230, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza21"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 231, "Embalaža kozmetike", "Plastična embalaža kozmetike sodi v zabojnik za plastiko.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza22"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 232, "Embalaža kozmetike", "Plastična embalaža kozmetike sodi v zabojnik za plastiko.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza23"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 233, "Embalaža kozmetike", "Plastična embalaža kozmetike sodi v zabojnik za plastiko.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza24"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 234, "Embalaža kozmetike", "Plastična embalaža kozmetike sodi v zabojnik za plastiko.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza25"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 235, "Embalaža kozmetike", "Plastična embalaža kozmetike sodi v zabojnik za plastiko.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza26"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 236, "Embalaža kozmetike", "Plastična embalaža kozmetike sodi v zabojnik za plastiko.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza27"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 237, "Embalaža kozmetike", "Plastična embalaža kozmetike sodi v zabojnik za plastiko.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza28"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 238, "Konzerva", desc[4], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza29"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 239, "Konzerva", desc[4], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza30"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 240, "Konzerva", desc[4], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza31"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 241, "Konzerva", desc[4], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza32"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 242, "Konzerva", desc[4], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza33"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 243, "Pločevinka", desc[4], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza34"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 244, "Pločevinka", desc[4], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza35"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 245, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza36"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 246, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza37"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 247, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza38"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 248, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza39"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 249, "Plastična vrečka", "Plastične vrečke sodijo v zabojnik za embalažo.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza40"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 250, "Plastična vrečka", "Plastične vrečke sodijo v zabojnik za embalažo.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza41"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 251, "Plastična vrečka", "Plastične vrečke sodijo v zabojnik za embalažo.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza42"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 252, "Embalaža kozmetike", "Plastična embalaža kozmetike sodi v zabojnik za plastiko.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza10(1)"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 253, "Embalaža kozmetike", "Plastična embalaža kozmetike sodi v zabojnik za plastiko.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza11(1)"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 254, "Embalaža kozmetike", "Plastična embalaža kozmetike sodi v zabojnik za plastiko.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza12(1)"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 255, "Plastična vrečka", "Plastične vrečke sodijo v zabojnik za embalažo.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza44"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 256, "Plastična vrečka", "Plastične vrečke sodijo v zabojnik za embalažo.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza45"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 257, "Plastična vrečka", "Plastične vrečke sodijo v zabojnik za embalažo.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza46"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 258, "Plastičen lonček", "Plastični lončki sodijo v zabojnik za embalažo", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza47"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 259, "Plastičen lonček", "Plastični lončki sodijo v zabojnik za embalažo", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza48"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 260, "Plastičen lonček", "Plastični lončki sodijo v zabojnik za embalažo", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza49"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 261, "Plastičen lonček", "Plastični lončki sodijo v zabojnik za embalažo", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza50"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 262, "Plastičen lonček", "Plastični lončki sodijo v zabojnik za embalažo", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza51"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 263, "Plastičen lonček", "Plastični lončki sodijo v zabojnik za embalažo", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza52"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 264, "Plastičen lonček", "Plastični lončki sodijo v zabojnik za embalažo", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza53"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 265, "Plastičen lonček", "Plastični lončki sodijo v zabojnik za embalažo", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza54"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 266, "Plastičen lonček", "Plastični lončki sodijo v zabojnik za embalažo", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza55"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 267, "Plastičen lonček", "Plastični lončki sodijo v zabojnik za embalažo", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza56"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 268, "Ovitek za CD", "Plastični ovitki sodijo v zabojnik za embalažo.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza57"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 269, "Ovitek za CD", "Plastični ovitki sodijo v zabojnik za embalažo.", Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza58"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 270, "Tetrapak", desc[5], Resources.Load<Sprite>("Images/Trashes/Plastic/embalaza63"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 271, "Pločevinka", desc[4], Resources.Load<Sprite>("Images/Trashes/Plastic/konzerva"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 272, "Pločevinka", desc[4], Resources.Load<Sprite>("Images/Trashes/Plastic/konzerva1"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 273, "Konzerva", desc[4], Resources.Load<Sprite>("Images/Trashes/Plastic/konzerva2"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 274, "Pločevinka", desc[4], Resources.Load<Sprite>("Images/Trashes/Plastic/konzerva3"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 275, "Konzerva", desc[4], Resources.Load<Sprite>("Images/Trashes/Plastic/konzerva4"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 276, "Pločevinka", desc[4], Resources.Load<Sprite>("Images/Trashes/Plastic/konzerva5"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 277, "Pločevinka", desc[4], Resources.Load<Sprite>("Images/Trashes/Plastic/konzerva6"), 12));
        trashes.Add(new Trash(TrashType.PLASTIC, 278, "Plastenka", desc[3], Resources.Load<Sprite>("Images/Trashes/Plastic/plastenka"), 12));
        #endregion

        #region GLASS
        trashes.Add(new Trash(TrashType.GLASS, 301, "Kozarec", desc[7], Resources.Load<Sprite>("Images/Trashes/Glass/kozarec01"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 302, "Kozarec", desc[7], Resources.Load<Sprite>("Images/Trashes/Glass/kozarec02"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 303, "Steklenica", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklenica"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 304, "Steklenica", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklenica01"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 305, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo24"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 306, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo1"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 307, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo2"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 308, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo3"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 309, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo4"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 310, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo5"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 311, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo6"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 312, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo7"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 313, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo8"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 314, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo9"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 315, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo10"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 316, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo11"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 317, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo12"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 318, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo13"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 319, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo14"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 320, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo15"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 321, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo16"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 322, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo17"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 323, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo18"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 324, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo19"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 325, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo20"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 326, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo21"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 327, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo22"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 328, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo23"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 329, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo31"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 330, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo25"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 331, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo26"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 332, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo27"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 333, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo28"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 334, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo29"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 335, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo30"), 20));
        trashes.Add(new Trash(TrashType.GLASS, 336, "Steklo", desc[6], Resources.Load<Sprite>("Images/Trashes/Glass/steklo"), 20));
        #endregion

        #region PAPER
        //trashes.Add(new Trash(TrashType.PAPER, 401, "Bankovci", "desc", Resources.Load<Sprite>("Images/Trashes/Paper/bankovci"), 10));
        //trashes.Add(new Trash(TrashType.PAPER, 402, "Časopis", "desc", Resources.Load<Sprite>("Images/Trashes/Paper/papir"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 403, "Škatla", "desc", Resources.Load<Sprite>("Images/Trashes/Paper/papir18"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 404, "Etiketa", "desc", Resources.Load<Sprite>("Images/Trashes/Paper/popust"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 405, "Časopis", desc[8], Resources.Load<Sprite>("Images/Trashes/Paper/novice1"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 406, "Časopis", desc[8], Resources.Load<Sprite>("Images/Trashes/Paper/novice2"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 407, "Časopis", desc[8], Resources.Load<Sprite>("Images/Trashes/Paper/novice3"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 408, "Časopis", desc[8], Resources.Load<Sprite>("Images/Trashes/Paper/novice4"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 409, "Časopis", desc[8], Resources.Load<Sprite>("Images/Trashes/Paper/novice5"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 410, "Časopis", desc[8], Resources.Load<Sprite>("Images/Trashes/Paper/novice6"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 411, "Časopis", desc[8], Resources.Load<Sprite>("Images/Trashes/Paper/novice7"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 412, "Časopis", desc[8], Resources.Load<Sprite>("Images/Trashes/Paper/novice8"), 10));
        // trashes.Add(new Trash(TrashType.PAPER, 413, "Časopis", "desc", Resources.Load<Sprite>("Images/Trashes/Paper/novice9"), 10)); novice9 jih ni??
        trashes.Add(new Trash(TrashType.PAPER, 414, "Časopis", desc[8], Resources.Load<Sprite>("Images/Trashes/Paper/novice10"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 415, "Časopis", desc[8], Resources.Load<Sprite>("Images/Trashes/Paper/novice11"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 416, "Časopis", desc[8], Resources.Load<Sprite>("Images/Trashes/Paper/novice12"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 417, "Časopis", desc[8], Resources.Load<Sprite>("Images/Trashes/Paper/novice13"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 418, "Časopis", desc[8], Resources.Load<Sprite>("Images/Trashes/Paper/novice14"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 419, "Časopis", desc[8], Resources.Load<Sprite>("Images/Trashes/Paper/novice15"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 420, "Papirnata vrečka", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir1"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 421, "Papirnata vrečka", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir2"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 422, "Papirnata vrečka", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir3"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 423, "Papirnata vrečka", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir4"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 424, "Papirnata vrečka", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir5"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 425, "Papirnata vrečka", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir6"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 426, "Papirnata vrečka", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir7"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 427, "Papirnata vrečka", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir8"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 428, "Papirnata vrečka", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir9"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 429, "Papirnata vrečka", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir10"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 430, "Papirnata vrečka", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir11"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 431, "Papirnata vrečka", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir12"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 432, "Papir", desc[13], Resources.Load<Sprite>("Images/Trashes/Paper/papir13"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 433, "Papir", desc[13], Resources.Load<Sprite>("Images/Trashes/Paper/papir14"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 434, "Škatla", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir15"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 435, "Škatla", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir16"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 436, "Škatla", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir17"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 437, "Škatla", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir18"), 10));
        trashes.Add(new Trash(TrashType.PAPER, 438, "Ovojnica", desc[12], Resources.Load<Sprite>("Images/Trashes/Paper/papir19"), 10));
        #endregion

        #region OTHER
        trashes.Add(new Trash(TrashType.OTHER, 502, "Glavnik", desc[17], Resources.Load<Sprite>("Images/Trashes/Other/glavnik"), 20));
        trashes.Add(new Trash(TrashType.OTHER, 504, "Ogledalo", desc[17], Resources.Load<Sprite>("Images/Trashes/Other/ogledalo"), 20));
        trashes.Add(new Trash(TrashType.OTHER, 505, "Pas", desc[17], Resources.Load<Sprite>("Images/Trashes/Other/pas"), 20));
        trashes.Add(new Trash(TrashType.OTHER, 506, "Plenice", desc[15], Resources.Load<Sprite>("Images/Trashes/Other/plenice4"), 20));
        trashes.Add(new Trash(TrashType.OTHER, 507, "Plenice", desc[15], Resources.Load<Sprite>("Images/Trashes/Other/plenice3"), 20));
        trashes.Add(new Trash(TrashType.OTHER, 508, "Plenice", desc[15], Resources.Load<Sprite>("Images/Trashes/Other/plenice2"), 20));
        trashes.Add(new Trash(TrashType.OTHER, 509, "Plenice", desc[15], Resources.Load<Sprite>("Images/Trashes/Other/plenice1"), 20));
        trashes.Add(new Trash(TrashType.OTHER, 510, "Vrečka za sesalec", desc[18], Resources.Load<Sprite>("Images/Trashes/Other/sesalnik1"), 20));
        trashes.Add(new Trash(TrashType.OTHER, 511, "Vrečka za sesalec", desc[18], Resources.Load<Sprite>("Images/Trashes/Other/sesalnik2"), 20));
        trashes.Add(new Trash(TrashType.OTHER, 512, "Čevelj", desc[17], Resources.Load<Sprite>("Images/Trashes/Other/guma3"), 20));
        trashes.Add(new Trash(TrashType.OTHER, 513, "Čevelj", desc[17], Resources.Load<Sprite>("Images/Trashes/Other/guma4"), 20));
        trashes.Add(new Trash(TrashType.OTHER, 514, "Čevelj", desc[17], Resources.Load<Sprite>("Images/Trashes/Other/guma5"), 20));
        trashes.Add(new Trash(TrashType.OTHER, 515, "Čevelj", desc[17], Resources.Load<Sprite>("Images/Trashes/Other/guma6"), 20));
        #endregion

        // Fill randomTrashes (used for ZEN mode)
        randomTrashes = new List<Trash>();
        System.Random random = new System.Random();
        var result = trashes.OrderBy(item => random.Next());
        //Debug.Log(result);
        randomTrashes.AddRange(result);

    }

    public static float PopLast() {
        return activeTrashes[activeTrashes.Count - 1].m_transform.position.x;
    }

    public static void InitPrefabs()
    {
        if (!PlayerPrefs.HasKey("Counter"))
        {
            //{
            PlayerPrefs.SetInt("Counter", 0);

            // Inicializacija statistike zabojnikov
            for (int i = 0; i < 5; i++)
            {
                PlayerPrefs.SetInt("Bin " + i, 0);
            }
            PlayerPrefs.SetInt("Bin " + TrashType.OTHER, 0);

            // Inicializacija statistike smeti
            foreach (Trash t in Trashes.trashes)
            {
                PlayerPrefs.SetInt("Trash " + t.m_trash_index, 1);
            }
        }
    }


    public static void InitWeights()
    {
        int i = 0;
        int sum = 0;
        foreach (Trash t in Trashes.trashes)
        {
            weights.Add(PlayerPrefs.GetInt("Trash " + t.m_trash_index));
            sum += (int)weights[i];
            i++;
        }
        for (int j = 0; j < weights.Count; j++)
        {
            weights[j] /= sum;
        }
        //weights[0] = 0.5;
    }
}
