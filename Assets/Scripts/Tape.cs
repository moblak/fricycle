﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tape : MonoBehaviour {

    //public Game game = new Game();
    float parentRot;
    Vector3 start;
    void Start() {
        //v inspectorju je ta rotacija pod z, ker so Quad meshi rotirani za 90 stopinj
        parentRot = -transform.parent.rotation.eulerAngles.y;

        //doloci kam se trak resetira
        start = new Vector3(Mathf.Cos(parentRot * Mathf.Deg2Rad) * transform.localScale.x, 0, Mathf.Sin(parentRot * Mathf.Deg2Rad) * transform.localScale.x);
       

    }

    void Update () {

        //hitrost je v TrashType

        transform.Translate(/*Game.GetLevel() **/ Vector3.right * Game.speed *Time.deltaTime);
        if (transform.position.x > transform.localScale.x)
            transform.position = transform.position - 2*start;

      
    }
}
