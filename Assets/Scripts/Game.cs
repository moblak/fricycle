﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game
{


    public const float DEFAULT_SPEED = 2.5f;

    public static bool m_isPlaying = false;  // true ko se igra igra
    public static bool m_sounds = false;     // mute = true
    public static bool m_over = false;       // game over - konec igre
    public static int m_score = 0;           // score igre
    public static int m_level = 1;           // livel igre.. glede na to spremenljivko se nastavlja čas vrtenja traka in kaj za ene smeti bodo prihajale
    public static int m_to_next_level = 5;    //ko pade do 0 zamenja level
    public static float speed= DEFAULT_SPEED;
    public static float tapeWidth = -1;
    public static float tapeY = -1;
    public static float interval = 1.4f;
    public static Dictionary<int, int> m_wrong = new Dictionary<int, int>();
    public static int m_gameType = GameType.DEFAULT;
    public static int m_lives = 5;           
    public static int[,] matrix;

    #region Game type, lives
    public static int GetGameType() {
        return m_gameType;
    }
    public static void SetGametype(int a_gameType)
    {
        m_gameType = a_gameType;
        m_lives = 5;
    }
    public static int GetLives()
    {
        return m_lives;
    }
    public static int DecLives()
    {
        Debug.Log(m_lives);
        m_lives--;
        if (m_lives < 0)
        {
            m_lives = 0;
        }
        return m_lives;
    }

    public static void ResetLives()
    {
        m_lives = 3;
    }


    #endregion

    #region Score
    // Nastavimo trenutni rezultat
    public static void SetScore(int a_score) {
        m_score = a_score;
    }

    // Povečamo/zmanjšamo rezultat
    public static void IncScore(int a_step = 1) {
        m_score += a_step;
    }

    // Povečamo/zmanjšamo rezultat
    public static int GetScore() {
        return m_score;
    }

    // Nastavimo števec na 0
    public static void ResetScore() {
        m_score = 0;
    }
    #endregion

    #region Sounds
    public static void ToggleSounds() {
        m_sounds = !m_sounds;
    }
    public static bool GetSounds()
    {
        return m_sounds;
    }
    #endregion

    #region Wrong
    // Nastavimo trenutni rezultat
    public static void AddWrong(int a_binType, int a_trashType) {
        int current_game = PlayerPrefs.GetInt("Counter");

        // Za vsako igro shrani podakte posebaj
        PlayerPrefs.SetInt("Bin Type" + current_game, a_binType);
        PlayerPrefs.SetInt("Trash Type" + current_game, a_trashType);
        PlayerPrefs.SetInt("Score" + current_game, m_score);
        PlayerPrefs.SetInt("Level" + current_game, m_level);

        // Vsak bin in vsaka smet dodana +1 napaka
        int tmp = PlayerPrefs.GetInt("Bin " + a_binType);
        PlayerPrefs.SetInt("Bin " + a_binType, tmp+1);
        tmp = PlayerPrefs.GetInt("Trash " + a_trashType);
        PlayerPrefs.SetInt("Trash " + a_trashType, tmp + 1);

        /*Debug.Log("trash_type " + PlayerPrefs.GetInt("Trash Type" + current_game));
        Debug.Log("bin_type " + PlayerPrefs.GetInt("Bin Type" + current_game));
        Debug.Log("trash_type " + PlayerPrefs.GetInt("Trash Type" + current_game));
        Debug.Log("score " + PlayerPrefs.GetInt("Score" + current_game));
        Debug.Log("level " + PlayerPrefs.GetInt("Level" + current_game));
        foreach (Trash t in Trashes.trashes)
        {
            Debug.Log("trash" + t.m_trash_index + ": " +  PlayerPrefs.GetInt("Trash " + t.m_trash_index));
        }
        for (int i = 0; i < 5; i++)
        {
            Debug.Log("bin " + i + ": " + PlayerPrefs.GetInt("Bin " + i));
        }
        Debug.Log("Counter: " + current_game);
        */

        PlayerPrefs.SetInt("Counter", current_game+1);

        
        //m_wrong.Add(a_binType, a_trashType);
    }

    // Nastavimo števec na 0
    public static void ResetWrong() {
        m_wrong.Clear();
    }
    #endregion

    #region IsPlaying, Over
    // Konec igre
    static void GameOver() {
        m_over = true;
        m_isPlaying = false;
    }

    // Pause
    static void Pause() {
        m_isPlaying = false;
    }

    // Start game
    void StartGame() {
        m_isPlaying = true;
    }

    #endregion

    #region Level, Speed
    // Nastavimo livel igre
    public static int SetLevel(int a_level) {
        if (a_level < 1) a_level = 1; // check da ne gremo pod livel, ničle nočemo
        return (m_level = a_level);
    }

    public static void CheckLevel() {
        m_to_next_level--;
        if (m_to_next_level == 0) {
            SetLevel(m_level + 1);
            m_to_next_level = 5;
            //TODO
            speed += 0.3f;
            interval *= ((speed - 0.15f)/speed);
            
        }
    }

    public static void ResetSpeed() {
        speed = DEFAULT_SPEED;
    }
    
    // Preberemo livel igre
    public static int GetLevel() {

        return m_level;
    }
    #endregion


}
