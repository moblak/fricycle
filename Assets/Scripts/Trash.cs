﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trash  {

    public int m_trash_index;
    public string m_title;          // Hrani ime smeti
    public string m_description;    // Opis smeti
    public Sprite m_image;          // Slika smeti .PNG
    public int m_trashType = 0;     // TIP smeti -> TrashType.cs
    public Transform m_transform;   //referenca do unity objekta 
    public int m_weight;
    
    #region Constructor
    public Trash(int a_trashType, int a_trash_index, string a_title, string a_description, Sprite a_image, int a_weight) {
        m_trashType = a_trashType;
        m_trash_index = a_trash_index;
        m_title = a_title;
        m_description = a_description;
        m_image = a_image;
        m_weight = a_weight;


    }
    public Trash(Trash trash) {
        m_trashType = trash.m_trashType;
        m_trash_index = trash.m_trash_index;
        m_title = trash.m_title;
        m_description = trash.m_description;
        m_image = trash.m_image;
        m_weight = trash.m_weight;

    }
    #endregion

    #region GameObject
    public void setTransform(Transform a_tr) {
        m_transform = a_tr;
        m_transform.GetComponent<SpriteRenderer>().sprite = m_image;
    }

    #endregion

    #region Title
    // Nastavimo naslov
    public void SetTitle(string a_title) {
        m_title = a_title;
    }

    // Preberemo naslov
    public string GetTitle() {
        return m_title;
    }
    #endregion 

    #region Weight
    // Nastavimo naslov
    public void SetWeight(int a_weight) {
        m_weight = a_weight;
    }

    // Preberemo naslov
    public int GetWeight() {
        return m_weight;
    }
    #endregion 

    #region Description
    // Preberemo opis
    public string GetDescription() {
        return m_description;
    }
    #endregion 
}
