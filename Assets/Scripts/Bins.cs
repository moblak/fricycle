﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//razred parent-a od vseh smetnjakov
public class Bins : MonoBehaviour {
   
    Camera cam;
    public Canvas canvas;
	AudioSource audioSource;
	public AudioClip wrong;
	public AudioClip right;

    MenuControls menuControls;
 

	void PlaySound(AudioClip clip){
		
		audioSource.clip = clip;
		audioSource.Play ();
	}

    [Header("Prostor med smetnjaki")]
    public float pad = 0.06f;
    //list smetnjakov
    public List<Bin> m_bins = new List<Bin>();

    public Transform tape, tape2, box;
    Vector3 parentAngles;

    //v awake zato, da je dostopno drugim classom v Startu
    void Awake() {
        cam = Camera.main;

        parentAngles = transform.rotation.eulerAngles;
        InitializeBins();
    }

    void Start () {
		audioSource = transform.GetComponent<AudioSource> ();

        TrashGenerator.Init();
        menuControls = canvas.GetComponent<MenuControls>();
      
       
    }

    //nastavi smetnjake in trak
    void InitializeBins() {

       
       
        Bin plastika = new Bin(TrashType.PLASTIC, "Plastika", "Sem gre vse kar je plastično", Resources.Load<Sprite>("Images/Bins/BinPlastic"), null, transform.GetChild(0));
        Bin papir = new Bin(TrashType.PAPER, "Papir", "Sem gre vse kar je ", Resources.Load<Sprite>("Images/Bins/BinPaper"), null, transform.GetChild(1));
        Bin organsko = new Bin(TrashType.ORGANIC, "Organsko", "Sem gre vse kar je ", Resources.Load<Sprite>("Images/Bins/BinOrganic"), null, transform.GetChild(2));
        Bin steklo = new Bin(TrashType.GLASS, "Steklo", "Sem gre vse kar je ", Resources.Load<Sprite>("Images/Bins/BinGlass"), null, transform.GetChild(3));
        Bin ostalo = new Bin(TrashType.OTHER, "Ostalo", "Sem gre vse kar je ", Resources.Load<Sprite>("Images/Bins/BinOther"), null, transform.GetChild(4));
//        Bin odvoz = new Bin(TrashType.DANGEROUS, "Odvoz", "Sem gre vse kar je ", Resources.Load<Sprite>("Images/Bins/Truck"), null, transform.GetChild(5));
        
        // Dodamo v naš seznam
        m_bins.Add(plastika);
        m_bins.Add(papir);
        m_bins.Add(organsko);
        m_bins.Add(steklo);
        m_bins.Add(ostalo);
//        m_bins.Add(odvoz);
        //razporeditev smetnjakov

        int n = m_bins.Count;
        //width in height celotnega zaslona v units
        float height = cam.orthographicSize * 2;
        float width = height * Screen.width / Screen.height;

        float h=0, w=0;
        for (int i = 0; i < n; i++) {
            w = width/(n*2) + i * width/(n*2) * 2 - width/2;

            //zanalasc je size.y in sizeX 
            float sizeX = m_bins[i].m_transform.GetComponent<SpriteRenderer>().bounds.size.y;
            float ratio = width / sizeX * 1 / 6;

            m_bins[i].m_transform.localScale = new Vector3(ratio, ratio, 1) * (1-pad);

            h = -height/2  + sizeX / 2  * m_bins[i].m_transform.localScale.y; 

            m_bins[i].m_transform.localPosition = new Vector3(w, h, 0);
            sizeX = m_bins[i].m_transform.GetComponent<SpriteRenderer>().bounds.size.x;

            m_bins[i].m_transform.gameObject.AddComponent<BoxCollider>();

            

        }
        //nastavi trak, vzemi visino smetnjaka za polozaj traku
        SetTape(height/2 + 1.1f* h);
        print(height / 2 + h);

    }

    void SetTape(float h) {
        //1 trak
        Vector3 pos = new Vector3(0, h, 0);
        float ratio =2* cam.orthographicSize;
        tape.localScale = new Vector3(ratio * Screen.width / Screen.height * 1.5f, ratio/1.5f, 1);
        tape.localPosition = pos;
        //2 trak
        pos.x = -tape.localScale.x;
        tape2.localPosition = pos;
        tape2.localScale = tape.localScale;
        Game.tapeWidth = tape.localScale.y;
        Game.tapeY = pos.y;

        //nastavi skatlo, 547 hardcoded
        box.localScale = new Vector3(Game.tapeWidth/2 * 100 / 547f, Game.tapeWidth*100/547f, 1) * 1.25f;
        box.position = new Vector3(-8.45f, tape.position.y, 0);
    }

    bool Hit(Transform a_rayCast, Bin a_bin, int a_i) {
        //ce je klik na smetnjak i
        if (a_rayCast == a_bin.m_transform) {

            //ce je pravilno razvrstil
                GameObject g = Trashes.activeTrashes[0].m_transform.gameObject;

            Game.matrix[Trashes.activeTrashes[0].m_trashType-1, a_i]++;
            if (a_bin.m_trashType == Trashes.activeTrashes[0].m_trashType) {
                PlaySound(right);

                // Povečamo TrashCounter za ena, ko je smet pravilo uvrščena
                a_bin.IncreaseTrashCounter(1);
                //Debug.Log("Score =" + a_bin.GetTrashCounter() + "[" + a_bin.m_trashType.ToString() + "]");

                //animacija tresenja
                if (!a_bin.anim.isPlaying)
                    a_bin.anim.Play();

                Game.CheckLevel();

                // Povečamo score
                Game.IncScore(Game.GetLevel() * (int)Game.speed * Trashes.activeTrashes[0].GetWeight()); // Povečujemo za vrednost trenutnega livela. 


                if (a_bin.m_trashType == TrashType.ORGANIC) Game.IncScore(1); // Npr. dodatna točka za organske smeti
                if (a_bin.m_trashType == TrashType.OTHER) Game.IncScore(10);  // Npr. bonus točke za odvoz smeti

               // Debug.Log("Score =" + Game.GetScore());

                // Pobrišemo smet iz seznama in jo "animiramo"
                StartCoroutine(PutTrashIn(Trashes.activeTrashes[0].m_transform, a_i));
                Trashes.activeTrashes.RemoveAt(0);
                return true;
            }

        //gameover
        else {
                // ZEN mode
                //Vibration.Vibrate(200);
                PlaySound(wrong);
                if (Game.GetGameType() == GameType.ALL) {

                    Game.AddWrong(a_bin.m_trashType, Trashes.activeTrashes[0].m_trash_index); // Tu bi se zbirale napačno razvrščene smeti TODO optimize Dict.
                    Destroy(g);
                    Trashes.activeTrashes.RemoveAt(0);
                    
                    if (TrashGenerator.gameover && Trashes.activeTrashes.Count == 0) {
                        canvas.GetComponent<MenuControls>().ShowMatrix();
                        canvas.GetComponent<MenuControls>().ShowGameOverMenu();
                    }


                } else {

                    // DEFAULT, TEACHING mode
                    int lives = Game.DecLives();
                    menuControls.SetLives(lives);
                    if (lives == 0) {
                        //Debug.Log("Trash type: " + a_bin.m_trashType + " : " + Trashes.activeTrashes[0].m_trash_index);
                        Game.AddWrong(a_bin.m_trashType, Trashes.activeTrashes[0].m_trash_index); // Tu bi se zbirale napačno razvrščene smeti TODO optimize Dict.
                        menuControls.ShowGameOverMenu();

                        return false;
                    }
                    return false;
                    // TODO Pobrisat score, mogoče kar v MenuControls.ResetGame()
                }
            }
        }
        return false;
    }

    void RayCast() {
        //deluje tako, da castas zarek pravokotno na to kar gledas - ce kaj zadane ti to sporoci
		//Ta if stavek je dodan samo, da se to ne pregleduje več ko se menuji odprejo. TODO: Optimize
        if (menuControls.lastMenus.Count == 0 && Trashes.activeTrashes.Count > 0) {
            if (Input.GetMouseButtonDown (0)) {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit)) { 
                   // print(hit.collider.name);
					for (int i = 0; i < m_bins.Count; i++) {

                        // Ali zadenemo katerega od smetnjakov
                        if (Hit(hit.collider.transform, m_bins[i], i)) {

                        } 


					}
				}
			}

		}

    }

    IEnumerator PutTrashIn(Transform trash, int bin) {

        float t = 0;
        Vector3 temp = trash.position;
        Vector3 temp2 = trash.localScale;
        while (t <1) {
            trash.position = Vector3.Lerp(temp, m_bins[bin].m_transform.position, t);
            trash.localScale = Vector3.Lerp(temp2,Vector3.zero, t);
            t += Time.deltaTime;
            yield return null;
        }

        Destroy(trash.gameObject);

        if (Game.GetGameType() == GameType.ALL && Trashes.activeTrashes.Count==0) {
            if (TrashGenerator.gameover) {
                canvas.GetComponent<MenuControls>().ShowMatrix();
                canvas.GetComponent<MenuControls>().ShowGameOverMenu();
            }
        }

    }

    void Update () {
        RayCast();
    }



}
