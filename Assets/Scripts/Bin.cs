﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Bin {
    public string m_title;
    public string m_description;
    public Sprite m_imageOpen, m_imageClosed;
    public bool m_open = false;
    public Transform m_transform;
    public SpriteRenderer sr;
    public Animation anim;
    public int m_trashType = TrashType.UNDEFINED;
    public int m_trashCounter = 0;


    #region Constructor
    public Bin(int a_trashType, string a_title, string a_description, Sprite a_imageOpen, Sprite a_imageClosed, Transform a_transform) {
        m_trashType = a_trashType;
        m_title = a_title;
        m_description = a_description;
        m_imageOpen = a_imageOpen;

        if (a_imageClosed!=null)
            m_imageClosed = a_imageClosed;

        m_transform = a_transform;
        m_transform.gameObject.SetActive(true);

        sr = m_transform.GetComponent<SpriteRenderer>();
        anim = m_transform.GetComponent<Animation>();
        sr.sprite = a_imageOpen;

    }
    #endregion

    #region Title
    // Nastavimo naslov
    public void SetTitle(string a_title) {
        m_title = a_title;
    }
    // Preberemo naslov
    public string GetTitle() {
        return m_title;
    }
    #endregion

    #region TrashCounter
    // Nastavimo števec pravilno razvrščenih smeti
    public void SetTrashCounter(int a_count) {
        m_trashCounter = a_count;
    }

    // Vrnemo števec pravilno razvrščenih smeti
    public int GetTrashCounter() {
        return m_trashCounter;
    }

    // Povežamo za ... števec pravilno razvrščenih smeti
    public void IncreaseTrashCounter(int a_step) {
        m_trashCounter += a_step;
    }

    // Resetiramo števec pravilno razvrščenih smeti
    public void ResetTrashCounter() {
        m_trashCounter = 0;
    }
    #endregion


}